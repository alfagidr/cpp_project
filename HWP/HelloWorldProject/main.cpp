﻿#include <iostream>
#include<vector>

class animal
{
public:
	virtual void Voice()
	{
		std::cout << "Voice";
	}
};

class  Dog : public animal
{
public:
	void Voice() override
	{
		std::cout << "woof" << "\n";
	}
};

class  Cat : public animal
{
public:
	void Voice() override
	{
		std::cout << "myau" << "\n";
	}
};

class  Bird : public animal
{
public:
	void Voice() override
	{
		std::cout << "chirik" << "\n";
	}
};

class  cow : public animal
{
public:
	void Voice() override
	{
		std::cout << "muuu" << "\n";
	}
};
int main()
{
	int pr;
	animal *farm[10];
	for (int i = 0; i < 10; i++)
	{
		pr = rand() % 4;
		switch (pr)
		{
		case 0:
			farm[i] = new Dog;
			break;
		case 1:
			farm[i] = new Cat;
			break;
		case 2:
			farm[i] = new Bird;
			break;
		case 3:
			farm[i] = new cow;
			break;
		}
	}
	for (int i = 0; i<10; i++)
	{
		farm[i]->Voice();
	}
}